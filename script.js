const startButton = document.getElementById('startButton');
const pauseButton = document.getElementById('pauseButton');
const stopButton = document.getElementById('stopButton');
const timerDisplay = document.getElementById('timer');
const totalTimeDisplay = document.getElementById('totalTime');
const currencyDisplay = document.getElementById('currency');
const styleButtons = document.querySelectorAll('.style-btn');

let timer;
let totalSeconds = 0;
let totalFocusTime = 0;
let virtualCurrency = 1000;
let purchasedStyles = [];
let isPaused = false;

function updateTimer() {
    if (!isPaused) {
        totalSeconds++;
        const minutes = Math.floor(totalSeconds / 60);
        const seconds = totalSeconds % 60;
        timerDisplay.textContent = `${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}`;
    }
}

function startTimer() {
    timer = setInterval(updateTimer, 1000);
    startButton.disabled = true;
    pauseButton.disabled = false;
    stopButton.disabled = false;
}

function pauseTimer() {
    isPaused = !isPaused;
    pauseButton.textContent = isPaused ? '继续' : '暂停';
}

function stopTimer() {
    clearInterval(timer);
    startButton.disabled = false;
    pauseButton.disabled = true;
    stopButton.disabled = true;
    const minutes = Math.floor(totalSeconds / 60);
    const seconds = totalSeconds % 60;
    totalSeconds = 0;
    totalFocusTime += minutes + seconds / 60;
    updateTotalTime();
    updateCurrency(minutes);
    isPaused = false;
    pauseButton.textContent = '暂停';
}

function updateTotalTime() {
    const totalMinutes = Math.floor(totalFocusTime);
    const totalSeconds = Math.round((totalFocusTime - totalMinutes) * 60);
    totalTimeDisplay.textContent = `总专注时长: ${String(totalMinutes).padStart(2, '0')}:${String(totalSeconds).padStart(2, '0')}`;
}

function updateCurrency(minutes) {
    const currencyEarned = minutes * 10;
    virtualCurrency += currencyEarned;
    currencyDisplay.textContent = `专注币: ${virtualCurrency}`;
}

function changeStyle(style, cost) {
    if (purchasedStyles.includes(style)) {
        document.body.className = style;
    } else if (virtualCurrency >= cost) {
        virtualCurrency -= cost;
        currencyDisplay.textContent = `专注币: ${virtualCurrency}`;
        document.body.className = style;
        purchasedStyles.push(style);
        markStyleAsPurchased(style);
    } else {
        alert("专注币不足，无法更换风格");
    }
}

function markStyleAsPurchased(style) {
    styleButtons.forEach(button => {
        if (button.dataset.style === style) {
            button.classList.add('purchased');
            button.textContent = `${button.dataset.style}（已购）`;
        }
    });
}

startButton.addEventListener('click', startTimer);
pauseButton.addEventListener('click', pauseTimer);
stopButton.addEventListener('click', stopTimer);

styleButtons.forEach(button => {
    button.addEventListener('click', () => {
        const style = button.dataset.style;
        const cost = parseInt(button.dataset.cost);
        changeStyle(style, cost);
    });
});

// 初始化总时长和专注币显示
updateTotalTime();
updateCurrency(0);
currencyDisplay.textContent = `专注币: ${virtualCurrency}`;
