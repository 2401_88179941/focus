# 专注正念计时器

![Logo](https://example.com/logo.png)

> 一个帮助用户专注工作和提升效率的计时器工具。

---

## 简介
**专注正念计时器** 是一款支持专注时长记录、奖励机制以及界面风格切换的工具。通过计时器，帮助用户保持专注，同时提供奖励机制以提高使用乐趣。

---

## 功能特色
- **计时功能**：开始、暂停、停止计时。
- **奖励机制**：记录专注时长，获取“专注币”。
- **风格切换**：支持多种界面颜色主题。
- **数据统计**：总计专注时间和获取奖励。

---

## 安装方法
1. 克隆项目到本地：
   http://localhost:63342/focus-app/index.html?_ijt=e74oueretdjou9desmqtvp6tph&_ij_reload=RELOAD_ON_SAVE
![1.png](https://raw.gitcode.com/2401_88179941/focus/attachment/uploads/2482ecbe-98ef-4466-91ea-e1c707bcb0b7/1.png '1.png')